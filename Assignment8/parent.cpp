#include "globals.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
using namespace std;
//https://www.mkompf.com/cplus/posixlist.html

//mkdir subdir1
//mkdir subdir2
//cd subdir1
//echo "Hello, world!" >> file1.txt
//echo "Hello, world!" >> file1.txt
//echo "Hello, world!" >> file1.txt
//echo "Hello, world!" >> file1.txt
//echo "Hello, world!" >> file1.txt
//cp file1.txt file2.txt
//mv file2.txt ../subdir2/file2.txt
//cd ../subdir2
//ln file2.txt file3.txt
//ln -s file2.txt file2s.txt
//cd ../subdir1
//rm file1.txt
//cd ..
//rmdir subdir1
//cd subdir2
//chmod a+rw file2.txt
//cat file2s.txt

const int TOTAL_COMMAND = 30;

bool run_command(int idx) {
    bool will_print = true;
    string argument = "";
    switch (idx) {
        case 0: {
            argument = "subdir1";
            mkdtemp((char *) argument.c_str());
            printf("---------- ");
            printf("mkdir subdir1\n");
            break;
        }
        case 1: {
            argument = "subdir2";
            mkdtemp((char *) argument.c_str());
            printf("---------- ");
            printf("mkdir subdir2\n");
            break;
        }
        case 2: {
            argument = "subdir1";
            chdir((char *) argument.c_str());
            printf("---------- ");
            printf("cd subdir1\n");
            will_print = false;
            break;
        }
        case 3:
        case 4:
        case 5:
        case 6:
        case 7: {
            // open file file1.txt with append mode --> write
            ofstream myfile;
            myfile.open("file1.txt", ios::app);
            myfile << "Hello, world!\n";
            myfile.close();
            printf("---------- ");
            printf("echo \"Hello, world!\" >> file1.txt\n");
            break;
        }
        case 8: {
            ifstream infile("file1.txt");
            ofstream outfile("file2.txt");
            string content = "";
            int i;
            for (i = 0; infile.eof() != true; i++) // get content of infile
                content += infile.get();
            i--;
            content.erase(content.end() - 1);
            outfile << content;
            infile.close();
            outfile.close();
            printf("---------- ");
            printf("cp file1.txt file2.txt\n");
            break;
        }
        case 9: {
            string oldpath = "file2.txt";
            string newpath = "../subdir2/file2.txt";
            rename(oldpath.c_str(), newpath.c_str());
            printf("---------- ");
            printf("mv file2.txt ../subdir2/file2.txt\n");
            break;
        }
        case 10: {
            argument = "../subdir2";
            chdir((char *) argument.c_str());
            printf("---------- ");
            printf("cd ../subdir2\n");
            will_print = false;
            break;
        }
        case 11: {
            string oldpath = "file2.txt";
            string newpath = "file3.txt";
            link(oldpath.c_str(), newpath.c_str());
            printf("---------- ");
            printf("ln file2.txt file3.txt\n");
            break;
        }
        case 12: {
            string oldpath = "file2.txt";
            string newpath = "file2s.txt";
            symlink(oldpath.c_str(), newpath.c_str());
            printf("---------- ");
            printf("ln -s file2.txt file2s.txt\n");
            break;
        }
        case 13: {
            argument = "../subdir1";
            chdir((char *) argument.c_str());
            printf("---------- ");
            printf("cd ../subdir1\n");
            will_print = false;
            break;
        }
        case 14: {
            string arg = "file1.txt";
            unlink(arg.c_str());
            printf("---------- ");
            printf("rm file1.txt\n");
            break;
        }
        case 15: {
            argument = "..";
            chdir((char *) argument.c_str());
            printf("---------- ");
            printf("cd ..\n");
            will_print = false;
            break;
        }
        case 16: {
            string arg = "subdir1";
            rmdir(arg.c_str());
            printf("---------- ");
            printf("rmdir subdir1\n");
            break;
        }
        case 17: {
            argument = "subdir2";
            chdir((char *) argument.c_str());
            printf("---------- ");
            printf("cd subdir2\n");
            will_print = false;
            break;
        }
        case 18: {
            string arg = "file2.txt";
            chmod(arg.c_str(), 0666);
            printf("---------- ");
            printf("chmod a+rw file2.txt\n");
            break;
        }
        case 19: {
            FILE *fp;
            char buffer[BUFFER_SIZE];

            fp = popen("cat file2s.txt", "r");
            if (fp != NULL)
            {
                while (fgets(buffer, BUFFER_SIZE, fp) != NULL)
                    printf("%s", buffer);
                pclose(fp);
            }
            printf("\n---------- ");
            printf("cat file2s.txt\n");
            break;
        }
        default:
            will_print = false;
            break;

    }

    return will_print;
}


void parent(int fd[])
{
    sleep(1);
    char buffer[BUFFER_SIZE];


    // Close the unused WRITE end of the pipe.
    close(fd[WRITE_END]);
    bool isWrite = false;

    // Read from the READ end of the pipe.
    ssize_t size;
    int retries = 0;
    fcntl(fd[READ_END], F_SETFL, O_NONBLOCK);
    bool will_print = false;
    for (int i=0; i< TOTAL_COMMAND; i++)
    {
        // RUN COMMAND FIRST
        will_print = run_command(i);

        while (will_print && retries <= 5) {
            size = read(fd[READ_END], buffer, BUFFER_SIZE);
            if (size <= 0) {
                retries++;
                sleep(1);
            }
            else {
                retries = 0;
                if (size < BUFFER_SIZE) {
                    buffer[size] = '\0';
                }
                printf("%s", buffer);
                break;
            }
        }
        if (retries >= 5) {
            break;
        }
    }

    // Close the READ end of the pipe.
    close(fd[READ_END]);
    printf("DONE \n");
}

