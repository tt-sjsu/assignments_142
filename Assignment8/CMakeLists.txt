cmake_minimum_required(VERSION 3.1)

# set the project name and version
project(HTMLBuilder)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)


add_executable(
        Assignment8
        main.cpp
        child.cpp
        parent.cpp
        globals.h
)