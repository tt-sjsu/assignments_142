#include "globals.h"
#include <fstream>
#include <string>
#include <sstream>
#include <unistd.h>
using namespace std;

void child(int fd[])
{
    string buff;

    // Close the unused READ end of the pipe.
    close(fd[READ_END]);
    dup2(fd[1], 1);
    string bash = "/usr/local/bin/fswatch";
    int res = execl(bash.c_str(),  "", "-xtr", ".");
//    int res = execl(bash.c_str(),  "-xtr", "/Users/thaoton/Desktop");

    close(fd[WRITE_END]);
}
