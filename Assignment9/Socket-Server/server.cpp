#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "../globals.h"

int main(int argc, char *argv[])
{
    int random_sleep;
    struct sockaddr_in server_address;
    memset(&server_address, '0', sizeof(server_address));

    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("*** Error: socket()");
        return -1;
    }

    server_address.sin_family      = AF_INET;
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    server_address.sin_port        = htons(SERVER_PORT);

    if (bind(socket_fd, (struct sockaddr*)&server_address,
                        sizeof(server_address)) < 0)
    {
        perror("*** Error: bind()");
        return -2;
    }

    if (listen(socket_fd, MAX_CONNECTIONS) < 0)
    {
        perror("*** Error: listen()");
        return -3;
    }

    char buffer[BUFFER_SIZE];
    memset(buffer, '0', sizeof(buffer));

    while (1)
    {
        printf("Server waiting to connect ... \n"); fflush(stdout);

        int connection_fd = accept(socket_fd, (struct sockaddr*)NULL, NULL);
        if (connection_fd < 0)
        {
            perror("*** Error: accept()");
            return -4;
        }

        time_t ticks = time(NULL);
//        sprintf(buffer, "%s", ctime(&ticks));
        sprintf(buffer, "%s", ctime(&ticks));
        write(connection_fd, buffer, strlen(buffer));

        printf("sent %s", buffer);
        close(connection_fd);

        /* generate secret number between 1 and 5: */
        random_sleep = rand() % 5 + 1;
        sleep(random_sleep);
     }
}
