#include "../globals.h"
#include <fstream>
#include <string>
#include <sstream>
#include <unistd.h>
#include <iostream>
using namespace std;

void child2(int fd[])
{
    char buff[BUFFER_SIZE];
    string input_line;
    int size = -1;
    time_t ticks;
    // Close the unused READ end of the pipe.
    close(fd[READ_END]);
    while(cin) {
        ticks = time(NULL);
        getline(cin, input_line);
        size = sprintf(buff, "Child process #2: %s at time %s", input_line.c_str(),  ctime(&ticks));
        write(fd[WRITE_END], buff, size);
    };

    close(fd[WRITE_END]);
}
