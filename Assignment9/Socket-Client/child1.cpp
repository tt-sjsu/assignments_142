#include "../globals.h"
#include <fstream>
#include <string>
#include <sstream>
#include <unistd.h>
using namespace std;

void child1(int fd[])
{
    char buff[BUFFER_SIZE];
    int size = -1;
    // Close the unused READ end of the pipe.
    close(fd[READ_END]);
    int random_sleep, i = 1;
    time_t ticks;
    while(1) {
        ticks = time(NULL);
        size = sprintf(buff, "Child process #1: writes Message %d at time %s", i, ctime(&ticks));
        write(fd[WRITE_END], buff, size);
        /* generate secret number between 1 and 5: */
        random_sleep = rand() % 5 + 1;
        sleep(random_sleep);
        i++;
    }

    close(fd[WRITE_END]);
}
