#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "../globals.h"
#include <sys/types.h>
#include <signal.h>
#include "iostream"
#include <fstream>
#include "vector"
using namespace std;


int connect_socket(char *ip_address) {
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("*** Error: socket()");
        return -2;
    }

    struct sockaddr_in server_address;
    memset(&server_address, '0', sizeof(server_address));

    server_address.sin_family = AF_INET;
    server_address.sin_port   = htons(SERVER_PORT);

    if (inet_pton(AF_INET, ip_address, &server_address.sin_addr) <= 0)
    {
        perror("*** Error: inet_pton()");
        return -3;
    }

    if (connect(socket_fd,
                (struct sockaddr *)&server_address,
                sizeof(server_address)) < 0)
    {
        perror("*** Error: connect()");
        return -4;
    }
    return socket_fd;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        printf("*** Usage: <IP address of server>\n");
        return -1;
    }

    int socket_fd;
    socket_fd = connect_socket(argv[1]);
    ////////////////////////
    vector <int> fds;
    ssize_t size;
    int result, nread;

    fd_set read_set;
    struct timeval timeout;

    pid_t pid1, pid2;
    int fd1[2], fd2[2];  // file descriptors for the pipe
    char buffer[BUFFER_SIZE];
    // Create the pipe.
    pid_t pipe_id1 = pipe(fd1);
    pid_t pipe_id2 = pipe(fd2);
    if (pipe_id1 < 0)
    {
        fprintf(stderr,"pipe() failed");
        return pipe_id1;
    }
    if (pipe_id2 < 0)
    {
        fprintf(stderr,"pipe() failed");
        return pipe_id2;
    }

    // Fork a child process.
    pid1 = fork();
    if (pid1 > 0) {
        close(fd1[WRITE_END]);
        fds.push_back(fd1[READ_END]);
        pid2 = fork();
        if (pid2 > 0) {
            close(fd2[WRITE_END]);
            fds.push_back(fd2[READ_END]);
        }
        else if (pid2 == 0) child2(fd2);
        else
        {
            fprintf(stderr, "fork() failed");
            return pid2;
        }
    }
    else if (pid1 == 0) child1(fd1);
    else
    {
        fprintf(stderr, "fork() failed");
        return pid1;
    }

    uint read_idx = -1;
    // Loop to monitor file descriptors.


    socket_fd = connect_socket(argv[1]);
    fds.push_back(socket_fd);

    // 10 seconds.
    timeout.tv_sec = 10;
    timeout.tv_usec = 0;
    while (1)
    {
        read_idx = (read_idx + 1) % fds.size();

        FD_ZERO(&read_set);    // initialize to the empty set
        FD_SET(fds.at(read_idx), &read_set);  // set file descriptor 0 (stdin) into read_set

        result = select(FD_SETSIZE, &read_set, NULL, NULL, &timeout);

        switch (result)
        {
            // Nothing ready.
            case 0:
            {
//                printf("@");
//                fflush(stdout);
                break;
            }

                // Error.
            case -1:
            {
                perror("select");
                exit(1);
            }

                // Something is ready.
            default:
            {
                if (fds.at(read_idx) == socket_fd) {
                    // read from socket
                    memset(buffer, '0', sizeof(buffer));

                    int n = read(socket_fd, buffer, sizeof(buffer)-1);
                    if (n > 0)
                    {
                        buffer[n] = 0;
                        printf("TimeServer: %s", buffer);
                    }
                    else perror("*** Error: read()");
                    close(socket_fd);
                    fds.erase(fds.begin() + read_idx);
                    socket_fd = connect_socket(argv[1]);
                    fds.insert(fds.begin() + read_idx, socket_fd);
                }
                else {
                    // Is it file descriptor 0 (stdin)?
                    if (FD_ISSET(fds.at(read_idx), &read_set)) {
                        nread = read(fds.at(read_idx), buffer, BUFFER_SIZE);
                        buffer[nread] = 0;
                        printf("%s", buffer);
                    }
                }
                break;
            }
        }
    }


    return 0;
}
