#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/ioctl.h>

#define BUFFER_SIZE 128

int main()
{
    char buffer[BUFFER_SIZE];
    int result, nread;

    fd_set read_set;
    struct timeval timeout;

    // Loop to monitor file descriptors.
    while (1)
    {
        // 2.5 seconds.
        timeout.tv_sec = 2;
        timeout.tv_usec = 500000;

        FD_ZERO(&read_set);    // initialize to the empty set
        FD_SET(0, &read_set);  // set file descriptor 0 (stdin) into read_set

        result = select(FD_SETSIZE, &read_set, NULL, NULL, &timeout);

        switch (result)
        {
            // Nothing ready.
            case 0:
            {
//                printf("@");
//                fflush(stdout);
                break;
            }
            
            // Error.
            case -1:
            {
                perror("select");
                exit(1);
            }

            // Something is ready.
            default:
            {
                // Is it file descriptor 0 (stdin)?
                if (FD_ISSET(0, &read_set))
                {
                    nread = read(0, buffer, BUFFER_SIZE);
                    buffer[nread] = 0;
                    printf("Read %d characters from the keyboard: %s\n",
                           nread, buffer);
                }

                break;
            }
        }
    }
}

