#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

void INThandler(int);

int main(void)
{
    signal(SIGINT, INThandler);

    for (int i = 1; i <= 20; i++)
    {
        printf("Sleep #%d\n", i);
        sleep(1);
    }

    printf("\nDone!\n");
    return 0;
}

void INThandler(int sig)
{
     signal(sig, SIG_IGN);
     printf("\nDo you really want to quit? [y/n] ");

     int ch = getchar();
     if (ch == 'y' || ch == 'Y')
     {
         printf("\nProgram killed.\n");
         exit(0);
     }
     else
     {
         signal(SIGINT, INThandler);
         getchar();
     }
}
