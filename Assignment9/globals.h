#ifndef GLOBALS_H_
#define GLOBALS_H_

#define BUFFER_SIZE     1024
#define SERVER_PORT     5000
#define MAX_CONNECTIONS   10
// File descriptors.
enum { READ_END = 0, WRITE_END = 1 };

void child1(int fd[]);
void child2(int fd[]);
#endif /* GLOBALS_H_ */
