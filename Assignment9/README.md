### Build project

```bash
    cmake build .
    make
```

#### First find IP address by running the command
* Copy the ipv4 ip address in the inet section, for example "<192.168.1.7">
```bash
    ifconfig en0
```

#### On one Terminal, run SERVER 
```bash
    ./Server
```

#### On one Terminal, run CLIENT
```bash
    ./Client <IP address> # ./Client 192.168.1.7
```

