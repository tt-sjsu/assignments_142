## To build
```bash
cmake build .
make
```

## To run
```bash
./Assignment6 0 #0=FIFO 1=LRU 2=LFU 3=MFU 4=RANDOM 5=OPTIMAL
```

## Using g++
```bash
 g++ -std=c++11  -I${PWD}/page_manager  ${PWD}/page_manager/*.cpp main.cpp -o Assignment6 
 ```

## Generate outputs
```bash
./Assignment6 0 > Outputs/FIFO.out.txt
./Assignment6 1 > Outputs/LRU.out.txt
./Assignment6 2 > Outputs/LFU.out.txt
./Assignment6 3 > Outputs/MFU.out.txt
./Assignment6 4 > Outputs/RANDOM.out.txt
./Assignment6 5 > Outputs/OPTIMAL.out.txt


```