//
// Created by Thao Ton on 3/14/21.
//

#ifndef ASSIGNMENT6_PAGEMANAGER_H
#define ASSIGNMENT6_PAGEMANAGER_H
#include <vector>
#include <string>
#include <algorithm>
#define MAX_SIZE_FRAME 3

using namespace std;

class PageManager {
protected:
    int hit_num;
public:
    int getHitNum() const;
    int getPageSwapNum() const;

protected:
    int page_swap_num;
    int request_total;
public:
    int getRequestTotal() const;
    void setAllPages(vector<int> all_pages);

protected:
    int current_page;
    string frame;
    string action;
    vector<int> all_pages;
    void set_action(int evicted, int entered);
    virtual void set_frames() {};
    virtual void process_page(int page_number) {};

public:
    PageManager();
    void get_page(int page_number);
    void print_result();
};


#endif //ASSIGNMENT6_PAGEMANAGER_H
