//
// Created by Thao Ton on 3/18/21.
//

#include "OPTIMAL.h"

PageManagerOPTIMAL::PageManagerOPTIMAL() {
    current_index = 0;
}

void PageManagerOPTIMAL::set_frames() {

    string ss;

    for (vector<int>::iterator it = page_frame.begin(); it != page_frame.end(); it++) {
        if (it != page_frame.begin()) {
            ss += ' '; // separator = ' '
        }
        ss += to_string((*it));
    }
    frame = ss;

}




void PageManagerOPTIMAL::process_page(int page_number) {
    int evicted = -1;
    int entered = -1;

    if ( find(page_frame.begin(), page_frame.end(), page_number) != page_frame.end() ){
        // found
        hit_num++;
    }
    else {
        page_swap_num++;
        if (page_frame.size() >= MAX_SIZE_FRAME) {
            int evicted_index = find_evicted_item();

            evicted = page_frame.at(evicted_index);
            page_frame.erase(page_frame.begin() + evicted_index);
        }

        page_frame.push_back(page_number);
        entered = page_number;
    }
    set_frames();
    set_action(evicted, entered);
    current_index++;
}

int PageManagerOPTIMAL::find_evicted_item() {
    int longest = -1;
    int evicted = -1;
    int distance;
    for (auto p = page_frame.begin(); p != page_frame.end(); p++) {
        vector<int>::iterator it = find(all_pages.begin() + current_index, all_pages.end(), (*p));
        if (it != all_pages.end()) {
            // found in the remaining items
            distance = it - all_pages.begin() + current_index;
            if (distance > longest) {
                longest = distance;
                evicted = p - page_frame.begin();
            }
        }
        else {
            // not found in the remaining item
            // -> wont be use in the future
            return p - page_frame.begin();
        }
    }
    return evicted;
}
