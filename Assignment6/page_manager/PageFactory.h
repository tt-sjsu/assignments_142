//
// Created by Thao Ton on 3/14/21.
//

#ifndef ASSIGNMENT6_PAGEFACTORY_H
#define ASSIGNMENT6_PAGEFACTORY_H
#include "PageManager.h"

enum STRATEGY {
    FIFO = 0,
    LRU,
    LFU,
    MFU,
    RANDOM,
    OPTIMAL
};

class PageFactory {
public:
    PageManager* getManager(STRATEGY strategy);
};


#endif //ASSIGNMENT6_PAGEFACTORY_H
