//
// Created by Thao Ton on 3/14/21.
//

#ifndef ASSIGNMENT6_FIFO_H
#define ASSIGNMENT6_FIFO_H
#include "PageManager.h"

class PageManagerFIFO : public PageManager {
public:
    void process_page(int page_number) override;

private:
    vector <int> page_frame;
    void set_frames() override;
};


#endif //ASSIGNMENT6_FIFO_H
