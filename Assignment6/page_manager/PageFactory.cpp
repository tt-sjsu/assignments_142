//
// Created by Thao Ton on 3/14/21.
//

#include "PageFactory.h"
#include "FIFO.h"
#include "LRU.h"
#include "LFU.h"
#include "MFU.h"
#include "RANDOM.h"
#include "OPTIMAL.h"


PageManager* PageFactory::getManager(STRATEGY strategy) {
    PageManager *m;

    if (strategy == STRATEGY::FIFO) {
        m = new PageManagerFIFO();
    }
    else if(strategy == STRATEGY::LRU) {
        m = new PageManagerLRU();
    }
    else if(strategy == STRATEGY::LFU) {
        m = new PageManagerLFU();
    }
    else if(strategy == STRATEGY::MFU) {
        m = new PageManagerMFU();
    }
    else if(strategy == STRATEGY::RANDOM) {
        m = new PageManagerRANDOM();
    }
    else if(strategy == STRATEGY::OPTIMAL) {
        m = new PageManagerOPTIMAL();
    }
    else {
        m = new PageManagerFIFO();
    }

    return m;
}
