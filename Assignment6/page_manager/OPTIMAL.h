//
// Created by Thao Ton on 3/18/21.
//

#ifndef ASSIGNMENT6_OPTIMAL_H
#define ASSIGNMENT6_OPTIMAL_H
#include "PageManager.h"

class PageManagerOPTIMAL : public PageManager {
public:
    PageManagerOPTIMAL();
    void process_page(int page_number) override;

private:
    int current_index;
    vector <int> page_frame;
    void set_frames() override;

    int find_evicted_item();
//    vector<int> history;
};


#endif //ASSIGNMENT6_OPTIMAL_H
