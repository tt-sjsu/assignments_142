//
// Created by minhh on 3/17/2021.
//

#include "RANDOM.h"
void PageManagerRANDOM::set_frames() {

    string ss;

    for (vector<int>::iterator it = page_frame.begin(); it != page_frame.end(); it++)
    {
        if (it != page_frame.begin())
        {
            ss += ' '; // separator = ' '
        }
        ss += to_string(*it);
    }
    frame = ss;
}

void PageManagerRANDOM::process_page(int page_number) {
    int evicted = -1;
    int entered = -1;
    //FIFO
    if ( find(page_frame.begin(), page_frame.end(), page_number) != page_frame.end() ){
        // found
        hit_num++;
    }
    else {
        page_swap_num++;
        if (page_frame.size() >= MAX_SIZE_FRAME) {
            int random = rand() % (MAX_SIZE_FRAME - 1) + 0; // 0 to size - 1
            evicted = page_frame.at(random);
            // Remove first in value
            page_frame.erase(page_frame.begin() + random);
        }
        // Push new value to FIFO
        page_frame.push_back(page_number);
        entered = page_number;
    }
    set_frames();
    set_action(evicted, entered);
}
