//
// Created by Thao Ton on 3/14/21.
//

#include "FIFO.h"

void PageManagerFIFO::set_frames() {

    string ss;

    for (vector<int>::iterator it = page_frame.begin(); it != page_frame.end(); it++)
    {
        if (it != page_frame.begin())
        {
            ss += ' '; // separator = ' '
        }
        ss += to_string(*it);
    }
    frame = ss;
}

void PageManagerFIFO::process_page(int page_number) {
    int evicted = -1;
    int entered = -1;
    //FIFO
    if ( find(page_frame.begin(), page_frame.end(), page_number) != page_frame.end() ){
        // found
        hit_num++;
    }
    else {
        page_swap_num++;
        if (page_frame.size() >= MAX_SIZE_FRAME) {
            evicted = page_frame.at(0);
            // Remove first in value
            page_frame.erase(page_frame.begin() + 0);
        }
        // Push new value to FIFO
        page_frame.push_back(page_number);
        entered = page_number;
    }
    set_frames();
    set_action(evicted, entered);
}
