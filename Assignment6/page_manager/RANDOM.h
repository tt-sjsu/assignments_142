//
// Created by minhh on 3/17/2021.
//

#ifndef ASSIGNMENT6_RANDOM_H
#define ASSIGNMENT6_RANDOM_H
#include "PageManager.h"

class PageManagerRANDOM : public PageManager {
public:
    void process_page(int page_number) override;

private:
    vector <int> page_frame;
    void set_frames() override;
};


#endif //ASSIGNMENT6_RANDOM_H
