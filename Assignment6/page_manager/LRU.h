

#ifndef ASSIGNMENT6_LRU_H
#define ASSIGNMENT6_LRU_H
#include "PageManager.h"

struct PageInfo {
    int value;
    long time;
};

class PageManagerLRU : public PageManager {
public:
    void process_page(int page_number) override;

private:
    vector <PageInfo> page_frame;
    void set_frames() override;
//    vector<int> history;
};


#endif //ASSIGNMENT6_LRU_H
