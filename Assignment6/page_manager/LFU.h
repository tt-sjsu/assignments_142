

#ifndef ASSIGNMENT6_LFU_H
#define ASSIGNMENT6_LFU_H
#include "PageManager.h"

struct PageLFUInfo {
    int value;
    int count;
    long time;
};

class PageManagerLFU : public PageManager {
public:
    void process_page(int page_number) override;

private:
    vector <PageLFUInfo> page_frame;
    void set_frames() override;
    int *frequency = new int[10];
};


#endif //ASSIGNMENT6_LFU_H
