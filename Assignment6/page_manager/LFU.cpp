
#include "LFU.h"
#include <chrono>
using namespace std::chrono;
void PageManagerLFU::set_frames() {

    string ss;

    for (vector<PageLFUInfo>::iterator it = page_frame.begin(); it != page_frame.end(); it++)
    {
        if (it != page_frame.begin())
        {
            ss += ' '; // separator = ' '
        }
        ss += to_string((*it).value);
    }
    frame = ss;
}


bool sort_by_highest_count(PageLFUInfo a, PageLFUInfo b) {
    if (a.count > b.count) {
        return true;
    }
    else if (a.count == b.count) {
        return a.time > b.time;
    }
    return false;
}


void PageManagerLFU::process_page(int page_number)
{
    int evicted = -1;
    int entered = -1;
    bool found = false;
    //LRU
    for (vector<PageLFUInfo>::iterator it = page_frame.begin(); it != page_frame.end(); it++)
    {
        if ((*it).value == page_number) {
            found = true;

            (*it).count++;
            (*it).time = duration_cast<nanoseconds>(system_clock::now().time_since_epoch()).count();
            hit_num++;
            break;
        }
    }

    if (!found) {
        page_swap_num++;
        if (page_frame.size() >= MAX_SIZE_FRAME) {
            sort(page_frame.begin(), page_frame.end(), sort_by_highest_count);
            evicted = page_frame[page_frame.size() - 1].value;
            page_frame.erase(page_frame.begin() + page_frame.size() - 1);
        }

        PageLFUInfo pageInfo{.value = page_number, .count = 1,
                             .time = duration_cast<nanoseconds>(system_clock::now().time_since_epoch()).count()};
        page_frame.push_back(pageInfo);
        entered = page_number;
    }
    sort(page_frame.begin(), page_frame.end(), sort_by_highest_count);
    set_frames();
    set_action(evicted, entered);
}