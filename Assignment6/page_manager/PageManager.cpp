//
// Created by Thao Ton on 3/14/21.
//

#include "PageManager.h"
#include <cstdio>
#include <iostream>


PageManager::PageManager(){
    hit_num = 0;
    page_swap_num = 0;
    request_total = 0;
}

void PageManager::set_action(int evicted, int entered){
    if (evicted != -1 && entered != -1) {
        char buff[100];
        snprintf(buff, sizeof(buff), "%d evicted, %d entered", evicted, entered);
        action = buff;
    } else {
        action = "";
    }
}
void PageManager::get_page(int page_number) {
//    printf("current page: %d\n", page_number);
    current_page = page_number;
    request_total++;

    process_page(page_number);
    print_result();
}

void PageManager::print_result() {
    printf("%d\t %s\t %s\n", current_page, frame.c_str(), action.c_str());
}

int PageManager::getHitNum() const {
    return hit_num;
}

int PageManager::getPageSwapNum() const {
    return page_swap_num;
}

int PageManager::getRequestTotal() const {
    return request_total;
}

void PageManager::setAllPages(vector<int> all_pages) {
    this->all_pages = all_pages;
}
