

#ifndef ASSIGNMENT6_MFU_H
#define ASSIGNMENT6_MFU_H
#include "PageManager.h"

struct PageMFUInfo {
    int value;
    int count;
    long time;
};

class PageManagerMFU : public PageManager {
public:
    void process_page(int page_number) override;

private:
    vector <PageMFUInfo> page_frame;
    void set_frames() override;
    int *frequency = new int[10];
};


#endif //ASSIGNMENT6_MFU_H
