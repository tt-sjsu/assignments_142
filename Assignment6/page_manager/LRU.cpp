

#include "LRU.h"
#include <chrono>
using namespace std::chrono;

void PageManagerLRU::set_frames()
{
    string ss;
    for (vector<PageInfo>::iterator it = page_frame.begin(); it != page_frame.end(); it++)
    {
        if(it != page_frame.begin())
        {
            ss += ' ';
        }
        ss += to_string((*it).value);
    }
    frame = ss;
}

bool sort_by_latest_time(PageInfo a, PageInfo b) {
    return a.time > b.time;
}


void PageManagerLRU::process_page(int page_number)
{
    int evicted = -1;
    int entered = -1;
    bool found = false;
    //LRU
    for (vector<PageInfo>::iterator it = page_frame.begin(); it != page_frame.end(); it++)
    {
        if ((*it).value == page_number) {
            found = true;

            (*it).time = duration_cast<nanoseconds>(system_clock::now().time_since_epoch()).count();
            hit_num++;
            break;
        }
    }

    if (!found) {
        page_swap_num++;
        if (page_frame.size() >= MAX_SIZE_FRAME) {
            sort(page_frame.begin(), page_frame.end(), sort_by_latest_time);
            evicted = page_frame[page_frame.size() - 1].value;
            page_frame.erase(page_frame.begin() + page_frame.size() - 1);
        }

        PageInfo pageInfo{.value = page_number, .time = duration_cast<nanoseconds>(system_clock::now().time_since_epoch()).count()};
        page_frame.push_back(pageInfo);
        entered = page_number;
    }
    sort(page_frame.begin(), page_frame.end(), sort_by_latest_time);
    set_frames();
    set_action(evicted, entered);
}