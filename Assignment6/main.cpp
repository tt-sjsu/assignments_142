#include <iostream>
#include <vector>
#include "PageManager.h"
#include "PageFactory.h"


using namespace std;
#define MAX_ITERATION 100
#define MAX_RETRIES 5
int get_next_page_number(int current_page) {
    int random_number = rand() % 9 + 0; // 0 to 9
    int next_page;
    switch (random_number) {
        case 0:
        case 1: {
            next_page = 1;
            break;
        }
        case 2:
        case 3: {
            next_page = current_page;
            break;
        }
        case 4:
        case 5: {
            next_page = current_page + 1;
            break;
        }
        case 6:
        case 7:
        case 8:
        case 9: {
            //random not i, i - 1, i + 1
            vector <int> valid_pages;
            for (int i = 0; i < 8; i++) {
                if (i != current_page || i != current_page - 1 || i!= current_page + 1) {
                    valid_pages.push_back(i);
                }
            }
            int random_number2 = rand() % (valid_pages.size() - 1) + 0; // 0 to size - 1
            next_page = valid_pages.at(random_number2);
            break;
        }
        default:{
            break;
        }

    }
    return next_page;
}

double get_stats(int hit_num, int page_swap_num, int request_total) {
    double hit_ratio = 100.0 * hit_num / request_total;
    printf("Hit Ratio:\t %.2f\n", hit_ratio);
    printf("Page Swap:\t %d\n", page_swap_num);
    return hit_ratio;
}

int main(int argc, char *argv[]) {
    int current_page = 0;
    PageManager *manager;
    PageFactory pageFactory;
    double hit_ratio = 0;
    int page_swap = 0;

    int n;

    vector<int> all_pages;
    STRATEGY strategy;
    int strategy_num = stoi(argv[1]);
    if (strategy_num > STRATEGY::OPTIMAL || strategy_num < STRATEGY::FIFO) {
        exit(-1);
    }
    else {
        strategy = (STRATEGY) strategy_num;
    }


    for (n = 0 ; n < MAX_RETRIES; n++) {

        for (int p = 0; p < MAX_ITERATION; p++) {
            current_page = get_next_page_number(current_page);
            all_pages.push_back(current_page);
        }

        manager = pageFactory.getManager(strategy);
        manager->setAllPages(all_pages);
        printf("Ref\t Frames\t Action\n");
        for(int i = 0; i < MAX_ITERATION; i++) {
            manager->get_page(all_pages[i]);
        }
        page_swap += manager->getPageSwapNum();
        hit_ratio += get_stats(manager->getHitNum(), manager->getPageSwapNum(), manager->getRequestTotal());
        printf("\n\n");
    }

    printf("Average Hit Ratio for %d times is: %.2f\n", n, hit_ratio / n);
    printf("Average Page Swap for %d times is: %.2f\n", n, 1.0 * page_swap / n);

    return 0;
}
