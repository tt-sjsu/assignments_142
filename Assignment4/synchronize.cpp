#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include "synchronize.h"

int INIT(pthread_mutex_t *mutex, char *name)
{
    int code = pthread_mutex_init(mutex, NULL);
    if (code != 0)
    {
        printf("*** Mutex '%s' init error %d!\n", name, code);
        exit(-11);
    }

    return code;
}

void LOCK(pthread_mutex_t *mutex, char *name)
{
    int code = pthread_mutex_lock(mutex);
    if (code != 0)
    {
        printf("*** Mutex '%s' lock error %d!\n", name, code);
        exit(-12);
    }
}

void UNLOCK(pthread_mutex_t *mutex, char *name)
{
    int code = pthread_mutex_unlock(mutex);
    if (code != 0)
    {
        printf("*** Mutex '%s' unlock error %d!\n", name, code);
        exit(-13);
    }
}

void REMOVE(char *name)
{
    if ((sem_unlink(name) == -1) && (errno != ENOENT))
    {
        printf("*** Semaphore '%s' unlink error!\n", name);
        exit(-21);
    }
}

sem_t *OPEN(char *name, int value)
{
    sem_t *sem = sem_open(name, O_CREAT|O_EXCL, 0666, value);
    if (sem == SEM_FAILED)
    {
        printf("*** Semaphore '%s' open error!\n", name);
        exit(-22);
    }

    return sem;
}
sem_t *OPEN_MODE(char *name, int mode)
{
    sem_t *sem = sem_open(name, mode);
    if (sem == SEM_FAILED)
    {
        printf("*** Semaphore '%s' open error!\n", name);
        exit(-22);
    }

    return sem;
}

void WAIT(sem_t *semaphore, char *name)
{
    if (sem_wait(semaphore) == -1)
    {
        printf("*** Semaphore '%s' wait error!\n", name);
        exit(-23);
    }
}

bool WAIT_TRY(sem_t *semaphore, char *name)
{
    return sem_trywait(semaphore) == 0;
}

void SIGNAL(sem_t *semaphore, char *name)
{
    if (sem_post(semaphore) == -1)
    {
        printf("*** Semaphore '%s' signal error!\n", name);
        exit(-24);
    }
}
