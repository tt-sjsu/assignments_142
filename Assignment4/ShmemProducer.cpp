

//
// Created by Thao Ton on 3/1/21.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <fcntl.h>
#include <unistd.h>
#include "synchronize.h"
#include "globals.h"
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
// Semaphores
sem_t *empty_slots_available;   // producer can insert
sem_t *filled_slots_available;  // consumer can remove

sem_t *write_lock;

// Critical regions.
void insert_item(int item);

int shm_fd;  // shared memory file descriptor
int shm_in; // shared memory file descriptor for input pointer
int main(int argc, char *argv[])
{

    if (argc != 3)
    {
        fprintf(stderr, "Usage: <# producers> <# items>\n");
        return -1;
    }
    // Create shared memory object and configure its size.
    shm_fd = shm_open(SHARED_BUFFER_FILE, O_CREAT | O_RDWR, 0666);

    shm_in = shm_open(SHARED_INPUT_POINTER, O_CREAT | O_RDWR, 0666);



    int id      = atoi(argv[1]);
    int max_items = atoi(argv[2]);

    // Process ID 1 will reset everything including mem map + semaphore
    if (id == 1) {
        int shm_out = shm_open(SHARED_OUTPUT_POINTER, O_CREAT | O_RDWR, 0666);
        ftruncate(shm_fd, SIZE);
        ftruncate(shm_in, sizeof(int));
        ftruncate(shm_out, sizeof(int));


        //Update in the the in-pointer
        void *in_ptr = mmap(0, sizeof(int), PROT_WRITE, MAP_SHARED, shm_in, 0);
        *((int *) in_ptr) = 0;


        //Update in the the in-pointer
        void *out_ptr = mmap(0, sizeof(int), PROT_WRITE, MAP_SHARED, shm_out, 0);
        *((int *) out_ptr) = 0;

        // Remove old semaphores.
        REMOVE(EMPTY_SEMAPHORE_NAME);
        REMOVE(FILLED_SEMAPHORE_NAME);
        REMOVE(WRITE_LOCK_SEMAPHORE);

        // Create new semaphores.
        empty_slots_available  = OPEN(EMPTY_SEMAPHORE_NAME, BUFFER_SIZE);
        filled_slots_available = OPEN(FILLED_SEMAPHORE_NAME, 0);

        write_lock = OPEN(WRITE_LOCK_SEMAPHORE, 1);
    }
    else {
        // Create new semaphores.
        empty_slots_available  = OPEN_MODE(EMPTY_SEMAPHORE_NAME, O_RDWR);
        filled_slots_available = OPEN_MODE(FILLED_SEMAPHORE_NAME, O_RDWR);

        write_lock = OPEN_MODE(WRITE_LOCK_SEMAPHORE, O_RDWR);
    };


    printf(" Empty semaphore opened: %p\n", (char *)empty_slots_available);
    printf("Filled semaphore opened: %p\n", (char *)filled_slots_available);

    for (int i = 0; i < max_items; i++) {
        usleep(100000);
        insert_item(id * 100 + i);
    }
    printf("Done!\n");
    return 0;
}



void insert_item(int item)
{
    WAIT(empty_slots_available, "empty_slots_available");
    WAIT(write_lock, "write_lock");

    // First get the in pointer
    //Memory map the shared input and read to it.
    void *in_ptr = mmap(0, sizeof(int), PROT_READ, MAP_SHARED, shm_in, 0);
    int in = *((int *) in_ptr);



    //second write to buffer, BUFFER[in] = item
    // Memory map the shared memory object and write to it.
    void *ptr = mmap(0, SIZE, PROT_WRITE, MAP_SHARED, shm_fd, 0);
    *((int *) ptr + in) = item;
    printf("Insert: %d to index %d\n", item, in);


    //Update in the the in-pointer
    in_ptr = mmap(0, sizeof(int), PROT_WRITE, MAP_SHARED, shm_in, 0);

    in = (in + 1)%BUFFER_SIZE;
    *((int *) in_ptr) = in;

    SIGNAL(write_lock, "write_lock");
    SIGNAL(filled_slots_available, "filled_slots_available");

}


