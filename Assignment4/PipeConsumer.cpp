#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include "globals.h"
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>
#include <errno.h>

int main(int argc, char *argv[])
{
    srand(time(0));
    int id      = atoi(argv[1]);

    printf("Starting consumer %d\n", id);

    printf("Opening...\n");
    // Open the FIFO with Read only Mode
    int fd = open(NAME, O_RDONLY);
    if(fd == -1)
    {
        printf("Failed to open the fifo.\n");
        return 1;
    }
    fcntl(fd, F_SETFL, O_NONBLOCK);
    printf("Opened.\n");
    int temp;

    int retries = 3;
    int check;
    while(retries)
    {
        // read the FIFO
        check = read(fd, &temp, sizeof(int));
        if (check > 0) {
            printf("Received: %d\n", temp);
            retries = 3;
        }
        else {
            if (check == 0 || errno == EAGAIN) {
                // read zero byte
                sleep(1);
                retries--;
            }
            else {
                printf("failure, exiting");
                exit(-1);
            }
        }


//        sleep(rand()%3);
    }
    printf("Done reading.\n");
    close(fd);
    return 0;
}