## TO build and run the project
```bash
cmake build .
make 
```

##Part 1
```bash
./Producer 1 50
./Producer 2 60
./Producer 3 70
./Consumer 1
./Consumer 2
```


##Part 2
```bash
./ShmemProducer 1 50
./ShmemProducer 2 60
./ShmemProducer 3 70
./ShmemConsumer 1
./ShmemConsumer 2
```