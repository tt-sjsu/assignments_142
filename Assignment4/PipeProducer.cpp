#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include "globals.h"
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[])
{
    int id = atoi(argv[1]);
    if(id == 1)
    {
        // Create name of pipe FIFO
        if (mkfifo(NAME, 0677) == -1)
        {
            if(unlink(NAME) == -1) {
                printf("Fail to unlink.\n");
            }
            printf("Unlinked.\n");
            if(mkfifo(NAME,0677) == -1) {
                printf("Fail to create.\n");
            }
        }
        printf("Created new fifo.\n");
    }

    printf("Opening...\n");

    // First open the FIFO in write only mode
    int fd = open(NAME, O_WRONLY);
    if(fd == -1)
    {
        printf("Failed to open the fifo.\n");
        return 1;
    }
    printf("Opened.\n");

    int size = atoi(argv[2]);


    for(int i = 0; i < size; i++)
    {
        int temp = id * 100 + i;
        printf("Sending: %d\n", temp);


        //write the message to the FIFO
        if(write(fd, &temp, sizeof(int)) == -1) {
            printf("Fail to send: %d\n", temp);
        }
        usleep(100000);
    }
    close(fd);
    printf("Done.\n");

    return 0;
}