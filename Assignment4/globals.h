//
// Created by Thao Ton on 3/1/21.
//

#ifndef ASSIGNMENT4_GLOBALS_H
#define ASSIGNMENT4_GLOBALS_H


const char *NAME = "/tmp/testfile1";
const char *SHARED_BUFFER_FILE = "/sharedbufferfile";
const char *SHARED_INPUT_POINTER = "/sharedinputpointer";
const char *SHARED_OUTPUT_POINTER = "/sharedoutputpointer";


#define BUFFER_SIZE 5
#define SIZE 5 * sizeof(int)

#define  EMPTY_SEMAPHORE_NAME "/empty"
#define FILLED_SEMAPHORE_NAME "/filled"
#define WRITE_LOCK_SEMAPHORE "/write_lock"


#endif //ASSIGNMENT4_GLOBALS_H
