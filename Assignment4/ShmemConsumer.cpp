
//
// Created by Thao Ton on 3/1/21.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <fcntl.h>
#include <unistd.h>
#include "synchronize.h"
#include "globals.h"
#include <sys/mman.h>
#include <sys/stat.h>



// Semaphores
sem_t *empty_slots_available;   // producer can insert
sem_t *filled_slots_available;  // consumer can remove


// Critical regions.
bool remove_item();

int max_items;
int shm_fd;  // shared memory file descriptor
int shm_out;
int main(int argc, char *argv[])
{

    if (argc != 2)
    {
        fprintf(stderr, "Usage: <# consumers>\n");
        return -1;
    }
    // Create shared memory object and configure its size.
    shm_fd = shm_open(SHARED_BUFFER_FILE, O_RDONLY, 0666);

    shm_out = shm_open(SHARED_OUTPUT_POINTER, O_RDWR, 0666);
    int id      = atoi(argv[1]);
    printf("Starting consumer %d\n", id);

    // Create new semaphores.
    empty_slots_available  = OPEN_MODE(EMPTY_SEMAPHORE_NAME, O_RDWR);
    filled_slots_available = OPEN_MODE(FILLED_SEMAPHORE_NAME, O_RDWR);

    printf(" Empty semaphore opened: %p\n", (char *)empty_slots_available);
    printf("Filled semaphore opened: %p\n", (char *)filled_slots_available);

//    int retries = 3;
//    while (retries ) {
//        if (remove_item()) {
//            retries = 3;
//        }
//        else {
////            printf("retrying....\n");
//            retries--;
//            sleep(1);
//        }
//    }

    while (1) {
        remove_item();
    }
    printf("\nNo messages to read....\n");
    printf("Done Reading!\n");
    return 0;
}



bool remove_item()
{
//    void *mmap(void *addr, size_t length, int prot, int flags,
//               int fd, off_t offset);

    if (WAIT_TRY(filled_slots_available, "empty_slots_available")) {
        // First get the in pointer
        //Memory map the shared input and read to it.
        void *out_ptr = mmap(0, sizeof(int), PROT_READ, MAP_SHARED, shm_out, 0);
        int out = *((int *) out_ptr);
        int item;

        //second write to buffer, BUFFER[in] = item
        // Memory map the shared memory object and write to it.
        void *ptr = mmap(0, SIZE, PROT_READ, MAP_SHARED, shm_fd, 0);
        item = *((int *) ptr + out);
        printf("Remove: %d to index %d\n", item, out);

        out = (out + 1)%BUFFER_SIZE;
        //Update in the the in-pointer
        out_ptr = mmap(0, sizeof(int), PROT_WRITE, MAP_SHARED, shm_out, 0);
        *((int *) out_ptr) = out;
        SIGNAL(empty_slots_available, "filled_slots_available");
        return true;
    }


    return false;
}


