// First come first serve
/*
 * Name : A B C D
 * Enter brust time
 * Average waiting time
 * Waiting time
 */
#include <iostream>
#include "process.h"
#include <vector>
#include "scheduler/scheduler.h"
#include "scheduler/schedulerFactory.h"


using namespace std;

string getStrategyName(STRATEGY strategy) {
    if (strategy == STRATEGY::FIRST_COME_FIRST_SERVE) {
        return "FIRST_COME_FIRST_SERVE";
    }
    else if (strategy == STRATEGY::SHORTEST_JOB_FIRST) {
        return "SHORTEST_JOB_FIRST";
    }
    else if (strategy == STRATEGY::SHORTEST_REMAINING_TIME_PREEMTIVE) {
        return "SHORTEST_REMAINING_TIME_PREEMTIVE";
    }
    else if (strategy == STRATEGY::ROUND_ROBIN) {
        return "ROUND_ROBIN";
    }
    else if (strategy == STRATEGY::HIGHEST_PRIORITY_FIRST_PREEMTIVE) {
        return "HIGHEST_PRIORITY_FIRST_PREEMTIVE";
    }
    else if (strategy == STRATEGY::HIGHEST_PRIORITY_FIRST_NON_PREEMTIVE) {
        return "HIGHEST_PRIORITY_FIRST_NON_PREEMTIVE";
    }
    return "";
}

template <typename T>

void printStatistic(vector<T> v, string message) {
    cout << message << endl;
    float sum = 0;
    for (int i = 0; i< v.size(); i++) {
        cout << v.at(i) << ", ";
        sum += v.at(i);
    }

    cout << "\naverage = " << sum / v.size() << endl;
    cout << endl;
}


int main() {

    schedulerFactory schedulerFactory;
    scheduler *scheduler;


    STRATEGY s;

    vector<float> waits;
    vector<float> turnArounds;
    vector<int> unUsedProcesses;
    vector<float> throughputs;


    for (int strategy = STRATEGY::FIRST_COME_FIRST_SERVE; strategy <= STRATEGY::HIGHEST_PRIORITY_FIRST_NON_PREEMTIVE; strategy++ )
    {
        cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
        STRATEGY s = static_cast<STRATEGY>(strategy);
        cout << "RUNNING STRATEGY " << getStrategyName(s) << endl;

        waits.clear();
        turnArounds.clear();
        unUsedProcesses.clear();
        throughputs.clear();
        for (int i = 0; i < 5; i++) {
            scheduler = schedulerFactory.getScheduler(s);
            scheduler->run();

            scheduler->printProceess();
            cout << "Average Wait Time: " << scheduler->getAverageWaitTime() << endl;
            cout << "Average Turn Around Time: " << scheduler->getTurnAroundTime() << endl;
            cout << "Unused Proceses Count: " << scheduler->countUnsedProcesses() << endl;
            cout << "Throughput: " << scheduler->getThroughput() << endl;
            scheduler->printTimeline();
            cout << endl;

            waits.push_back(scheduler->getAverageWaitTime());
            turnArounds.push_back(scheduler->getTurnAroundTime());
            unUsedProcesses.push_back(scheduler->countUnsedProcesses());
            throughputs.push_back(scheduler->getThroughput());
        }

//        printStatistic(waits, "waiting time");
//        printStatistic(turnArounds, "turn arounds time");
//        printStatistic(unUsedProcesses, "un-used Processes");
//        printStatistic(throughputs, "throughputs");


        cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << endl;
        cout << "\n\n\n" << endl;
    }

    return 0;
}
