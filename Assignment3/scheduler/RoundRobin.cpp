//
// Created by minhh on 2/22/2021.
//

#include "RoundRobin.h"

void RoundRobin::run() {
    queue<Process *> waitingList;
    sortProcesses(processes, sortByArrivalTimeFunction);
    int ran_quanta = 0;
    Process *p_to_run = nullptr;
    int current_process_index = 0;
    for (int time = 0; time < total_quanta * 2; time++) {

        while (current_process_index < processes->size() && processes->at(current_process_index).arrival_time <= time &&
            processes->at(current_process_index).arrival_time < total_quanta) {
            // see processes coming
            waitingList.push(&processes->at(current_process_index));
            current_process_index += 1;
        }
        if(!p_to_run and waitingList.size() > 0)
        {

            p_to_run = waitingList.front();
            waitingList.pop();

            if (time >= total_quanta) {
                while (p_to_run->start_time < 0 && waitingList.size() > 0) {
                    p_to_run = waitingList.front();
                    waitingList.pop();
                }
                if (p_to_run->start_time < 0) {
                    p_to_run = nullptr;
                }
            }
            if (p_to_run) {
                if(p_to_run->burst_time == p_to_run->burst_time_remain)
                {
                    p_to_run->start_time = time;
                }
                if(p_to_run->burst_time_remain - 1 > 0)
                {
                    p_to_run->burst_time_remain -= 1;
                    waitingList.push(p_to_run);
                }
                else {
                    p_to_run->end_time = time + p_to_run->burst_time_remain;
                    p_to_run->burst_time_remain = 0;
                }
                p_to_run->ran_time = time - p_to_run->start_time + p_to_run->burst_time_remain;
            }
        }
        if (time >= total_quanta and !p_to_run) {
            break;
        }
        timeline[time] = p_to_run ? p_to_run->name: '-';
        p_to_run = nullptr;
    }
}