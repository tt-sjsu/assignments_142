//
// Created by Thao Ton on 2/20/21.
//

#include "firstComeFirstServe.h"

void firstComeFirstServe::run() {
    // Sort process by arrvival time
    sortProcesses(processes, sortByArrivalTimeFunction);
    int quanta = 0;
    int ran_quanta = 0;

    //syntax to iterate vector (for loop)
    for(vector<Process>::iterator it = processes->begin(); it != processes->end(); it++) {


        while (quanta < it->arrival_time) {
            timeline[quanta] = '-'; //print "-" to timeline
            quanta += 1;
        }

        if (quanta > total_quanta - 1) {
            break;
        }

        it->start_time = quanta;

        ran_quanta = ceil(it->burst_time);
        for (int j = 0; j < ran_quanta; j++) {
            timeline[quanta + j] = it->name;
        }
        it->ran_time = it->burst_time;
        it->end_time = it->burst_time + it->start_time;
        quanta += ran_quanta; // Your process scheduler can do process switching only at the start of each time quantum
    }
    timeline[quanta] = '\0';
   // total_ran_quanta = min(quanta, MAX_QUANTA); // the last process can execced 100, but only use 100 to calculate the statistic
};