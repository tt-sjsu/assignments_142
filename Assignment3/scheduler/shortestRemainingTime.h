//
// Created by Thao Ton on 2/20/21.
//

#ifndef UNTITLED_SHORTESTREMAININGTIME_H
#define UNTITLED_SHORTESTREMAININGTIME_H

#include "scheduler.h"

class shortestRemainingTime : public scheduler {
public:
    void run() override;

private:
    static bool sortByBurstTimeRemainDecendantReference(Process *a, Process *b);
    void sortProcessesReference(vector<Process*> *p, bool sortFunction (Process *a, Process *b));
};


#endif //UNTITLED_SHORTESTREMAININGTIME_H
