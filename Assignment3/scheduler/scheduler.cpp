//
// Created by Thao Ton on 2/20/21.
//

#include "scheduler.h"

bool scheduler::sortByArrivalTimeFunction (Process a, Process b) {
    return a.arrival_time < b.arrival_time;
}

bool scheduler::sortByPriorityFunction (Process a, Process b) {
    return a.priority < b.priority;
}

bool scheduler::sortByBurstTimeDecendant (Process a, Process b) {
    return a.burst_time > b.burst_time;
}

void scheduler::createProcess () {
    for (int i = 0; i < PROCESS_NUM; i++) {
        Process proc;
        processes->push_back(proc);
    }

    for(vector<Process>::iterator it = processes->begin(); it != processes->end(); it++)    {
        it->name = (char) ('A' + it - processes->begin());
        it->arrival_time = static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(100))); // a float value from range 0 to 99
        it->burst_time =  0.1 + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(10-0.1))); // a float value from 0.1 through 10 quanta.
        it->priority = rand() % 4 + 1; // range 1 to 4
        it->burst_time_remain = it->burst_time;
        it->tmp = it->arrival_time;
    }

//    processes->at(0).arrival_time = 0;
//    processes->at(0).tmp = processes->at(0).arrival_time;
//    processes->at(0).burst_time = 4;
//    processes->at(0).burst_time_remain = processes->at(0).burst_time;
//    processes->at(0).priority = 3;
//
//
//    processes->at(1).arrival_time = 0;
//    processes->at(1).tmp = processes->at(1).arrival_time;
//    processes->at(1).burst_time = 5;
//    processes->at(1).burst_time_remain = processes->at(1).burst_time;
//    processes->at(1).priority = 2;
//
//    processes->at(2).arrival_time = 0;
//    processes->at(2).tmp = processes->at(2).arrival_time;
//    processes->at(2).burst_time = 8;
//    processes->at(2).burst_time_remain = processes->at(2).burst_time;
//    processes->at(2).priority = 2;
//
//
//    processes->at(3).arrival_time = 0;
//    processes->at(3).tmp = processes->at(3).arrival_time;
//    processes->at(3).burst_time = 7;
//    processes->at(3).burst_time_remain = processes->at(3).burst_time;
//    processes->at(3).priority = 1;
//
//
//    processes->at(4).arrival_time = 0;
//    processes->at(4).tmp = processes->at(4).arrival_time;
//    processes->at(4).burst_time = 3;
//    processes->at(4).burst_time_remain = processes->at(4).burst_time;
//    processes->at(4).priority = 3;
//
//    processes->erase(processes->begin() + 5, processes->begin() + 20);


}

void scheduler::printProceess() {
    char buffer [100];

    sprintf(buffer, "Processes: \tBurst time: \tArrival Time: \tPriority \tStart time \tEnd Time \tWait Time\n");
    printf("%s", buffer);


    for(vector<Process>::iterator it = processes->begin(); it != processes->end(); it++)    {

        if (it->start_time >= 0) {

            sprintf(buffer, "%c \t\t\t%.1f \t\t\t%.1f \t\t\t%d \t\t\t%d \t\t\t%.1f \t\t\t%.1f\n",
                    it->name, it->burst_time, it->arrival_time, it->priority, it->start_time, it->end_time,
                    it->start_time - it->arrival_time);
        }
        else {
            sprintf(buffer, "%c \t\t\t%.1f \t\t\t%.1f \t\t\t%d \t\t\tx \t\t\tx \t\t\t\tx\n",
                    it->name, it->burst_time, it->arrival_time, it->priority);
        }

        printf("%s", buffer);
    }
    printf("\n");
}

float scheduler::getAverageWaitTime() {

    int ran_process_num = 0;
    float total_wait_time = 0;

    for(vector<Process>::iterator it = processes->begin(); it != processes->end(); it++) {
        if (it->start_time >= 0) {
            ran_process_num += 1;
            total_wait_time += it->start_time - it->arrival_time;
        }
    }
    return total_wait_time / ran_process_num;
}

float scheduler::getTurnAroundTime() {

    int ran_process_num = 0;
    float total_turn_around_time = 0;

    for(vector<Process>::iterator it = processes->begin(); it != processes->end(); it++) {
        if (it->start_time >= 0) {
            ran_process_num += 1;
            total_turn_around_time += it->end_time - it->arrival_time;
        }
    }
    return total_turn_around_time / ran_process_num;
}

void scheduler::printTimeline() {
    printf("%s\n", timeline);
}

int scheduler::countUnsedProcesses() {
    int count = 0;
    for(vector<Process>::iterator it = processes->begin(); it != processes->end(); it++) {
        if (it->start_time < 0) {
            count += 1;
        }
    }
    return count;
}
int scheduler::countTotalRanQuanta() {
    int count = 0;
    for(vector<Process>::iterator it = processes->begin(); it != processes->end(); it++) {
        if (it->start_time >= 0) {
            count += ceil((*it).ran_time);
        }
    }
    return count;
}

float scheduler::getThroughput() {
    int commpleted_processes = processes->size() - countUnsedProcesses();
    float thoughtput = 1.0 * (commpleted_processes * MAX_QUANTA) / (countTotalRanQuanta() + 1); // quanta is index 0, count need to + 1

    return thoughtput;
}

scheduler::scheduler() {
    //total_ran_quanta = 0;
    processes = new vector<Process>() ;
    createProcess();
//    printProceess();
}

void scheduler::sortProcesses(vector<Process> *p, bool sortFunction (Process a, Process b)) {
    sort(p->begin(), p->end(), sortFunction); // sort fucntion
    for (vector<Process>::iterator it = p->begin(); it != p->end(); it++) {
        it->name = (char) ('A' + it - p->begin());
    }
}