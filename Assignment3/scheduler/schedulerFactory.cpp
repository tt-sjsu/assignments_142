//
// Created by Thao Ton on 2/24/21.
//

#include "schedulerFactory.h"

#include "firstComeFirstServe.h"
#include "shortestJobFirst.h"
#include "RoundRobin.h"
#include "shortestRemainingTime.h"
#include "highestPriorityFirstRound.h"
#include "highestPriorityFirstNonStop.h"

scheduler* schedulerFactory::getScheduler(STRATEGY strategy) {

    scheduler *sch;
    if (strategy == STRATEGY::FIRST_COME_FIRST_SERVE) {
        sch = new firstComeFirstServe();
    }
    else if (strategy == STRATEGY::SHORTEST_JOB_FIRST) {
        sch = new shortestJobFirst();
    }
    else if (strategy == STRATEGY::SHORTEST_REMAINING_TIME_PREEMTIVE) {
        sch = new shortestRemainingTime();
    }
    else if (strategy == STRATEGY::ROUND_ROBIN) {
        sch = new RoundRobin();
    }
    else if (strategy == STRATEGY::HIGHEST_PRIORITY_FIRST_PREEMTIVE) {
        sch = new highestPriorityFirstRound();
    }
    else if (strategy == STRATEGY::HIGHEST_PRIORITY_FIRST_NON_PREEMTIVE) {
        sch = new highestPriorityFirstNonStop();
    }
    return sch;
}
