//
// Created by minhh on 2/22/2021.
//

#ifndef UNTITLED_ROUNDROBIN_H
#define UNTITLED_ROUNDROBIN_H

#include "scheduler.h"

class RoundRobin : public scheduler {
public:
    void run() override;
};


#endif //UNTITLED_ROUNDROBIN_H
