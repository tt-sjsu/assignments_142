//
// Created by Thao Ton on 2/20/21.
//

#include "highestPriorityFirstNonStop.h"

void highestPriorityFirstNonStop::sortProcessesReference(vector<Process*> *p, bool sortFunction (Process *a, Process *b)) {
    sort(p->begin(), p->end(), sortFunction); // sort fucntion
}

void highestPriorityFirstNonStop::run(){
    queue<Process *> waitingList_1;
    queue<Process *> waitingList_2;
    queue<Process *> waitingList_3;
    queue<Process *> waitingList_4;
    queue<Process *> *waitingList;


    sortProcesses(processes, sortByArrivalTimeFunction);
    int ran_quanta = 0;
    Process *p_to_run = nullptr;
    int current_process_index = 0;
    for (int time = 0; time < total_quanta * 2; time++) {

        while (current_process_index < processes->size() && processes->at(current_process_index).arrival_time <= time) {
            // see processes coming
            switch (processes->at(current_process_index).priority)
            {
                case 1:
                    waitingList_1.push(&processes->at(current_process_index));
                    break;
                case 2:
                    waitingList_2.push(&processes->at(current_process_index));
                    break;
                case 3:
                    waitingList_3.push(&processes->at(current_process_index));
                    break;
                case 4:
                    waitingList_4.push(&processes->at(current_process_index));
            }
            current_process_index += 1;
        }

        if (p_to_run) {
            if(p_to_run->burst_time == p_to_run->burst_time_remain)
            {
                p_to_run->start_time = time;
            }

            if(p_to_run->burst_time_remain - 1 > 0)
            {
                p_to_run->burst_time_remain -= 1;
                p_to_run->ran_time = time - p_to_run->start_time + p_to_run->burst_time_remain;
            }
            else
            {
                p_to_run->end_time = time + p_to_run->burst_time_remain;
                p_to_run->burst_time_remain = 0;
                p_to_run->ran_time = time - p_to_run->start_time + p_to_run->burst_time_remain;
                p_to_run = nullptr;
            }
        }


        // Process aging
        while (true){
            if (!waitingList_4.empty() and (time - waitingList_4.front()->tmp) >= 5){
                waitingList_4.front()->tmp = time;
                waitingList_3.push(waitingList_4.front());
                waitingList_4.pop();
            } else break;
        }

        while (true){
            if (!waitingList_3.empty() and (time - waitingList_3.front()->tmp) >= 5){
                waitingList_3.front()->tmp = time;
                waitingList_2.push(waitingList_3.front());
                waitingList_3.pop();
            } else break;
        }

        while (true){
            if (!waitingList_2.empty() and (time - waitingList_2.front()->tmp) >= 5){
                waitingList_2.front()->tmp = time;
                waitingList_1.push(waitingList_2.front());
                waitingList_2.pop();
            } else break;
        }

        while (!p_to_run) {
            waitingList = nullptr;
            if(!waitingList_1.empty())
                waitingList = &waitingList_1;
            else if(!waitingList_2.empty())
                waitingList = &waitingList_2;
            else if(!waitingList_3.empty())
                waitingList = &waitingList_3;
            else if(!waitingList_4.empty())
                waitingList = &waitingList_4;
            if (waitingList) {
                p_to_run = waitingList->front();
                waitingList->pop();
                if (time >= total_quanta - 1 and p_to_run->start_time < 0) {
                    p_to_run = nullptr;
                }
            }
            else {
                break; // no item left on any queue
            }

        }
        if (time >= total_quanta - 1 and !p_to_run) {
            break;
        }
        timeline[time] = p_to_run ? p_to_run->name: '-';
    }
}