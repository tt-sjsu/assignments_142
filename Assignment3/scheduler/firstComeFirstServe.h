//
// Created by Thao Ton on 2/20/21.
//

#ifndef MAIN_CPP_FCFS_H
#define MAIN_CPP_FCFS_H

#include "scheduler.h"

class firstComeFirstServe : public scheduler {
public:
    void run() override;
};


#endif //MAIN_CPP_FCFS_H
