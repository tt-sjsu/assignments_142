//
// Created by Thao Ton on 2/20/21.
//

#ifndef UNTITLED_HIGHESTPRIORITYFIRSTNONSTOP_H
#define UNTITLED_HIGHESTPRIORITYFIRSTNONSTOP_H

#include "scheduler.h"
class highestPriorityFirstNonStop  : public scheduler {
public:
    void run() override;

private:
    static bool sortByPriorityTimeDecendantReference (Process *a, Process *b);
    void sortProcessesReference(vector<Process*> *p, bool sortFunction (Process *a, Process *b));
};



#endif //UNTITLED_HIGHESTPRIORITYFIRSTNONSTOP_H
