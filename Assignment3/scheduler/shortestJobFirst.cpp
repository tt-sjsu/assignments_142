//
// Created by Thao Ton on 2/20/21.
//

#include "shortestJobFirst.h"


bool shortestJobFirst::sortByBurstTimeDecendantReference (Process *a, Process *b) {
    if  (a->burst_time == b->burst_time) {
        return a->arrival_time > b->arrival_time;
    }
    else {
        return a->burst_time > b->burst_time;
    }
}

void shortestJobFirst::sortProcessesReference(vector<Process*> *p, bool sortFunction (Process *a, Process *b)) {
    sort(p->begin(), p->end(), sortFunction); // sort fucntion
}

void shortestJobFirst::run() {

    vector<Process*> waitingList;


    // Sort process by arrvival time
    sortProcesses(processes, sortByArrivalTimeFunction);
    int ran_quanta = 0;
    float temp;
    Process *p_to_run = nullptr;
    int current_process_index = 0;
    for (int time = 0; time < total_quanta * 2; time++ ) {

        //Update current process
        if (p_to_run) {
            temp = p_to_run->burst_time - p_to_run->ran_time;
            if (p_to_run->burst_time - p_to_run->ran_time > 1) {
                p_to_run->ran_time += 1;
            }
            else {
                p_to_run->ran_time = p_to_run->burst_time;
            }
            // Update total runtime for current process
            if (p_to_run->burst_time == p_to_run->ran_time) {
                p_to_run->end_time = (time - 1) + temp;
                p_to_run = nullptr;
            }
        }

        while (current_process_index < processes->size() && processes->at(current_process_index).arrival_time <= time) {
            // see processes coming
            waitingList.push_back(&processes->at(current_process_index));
            current_process_index += 1;
        }

        while (!p_to_run && waitingList.size() > 0) {
            sortProcessesReference(&waitingList, sortByBurstTimeDecendantReference);
            p_to_run = waitingList.at(waitingList.size() - 1);
            waitingList.pop_back();

            if (time >= total_quanta) {
                p_to_run = nullptr;
            }
            else {
                p_to_run->start_time = time;
            }
        }

        if (time >= total_quanta and !p_to_run) {
            break;
        }
        timeline[time] = p_to_run ? p_to_run->name: '-'; //print "-" to timeline

    }
};