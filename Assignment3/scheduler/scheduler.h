//
// Created by Thao Ton on 2/20/21.
//

#ifndef MAIN_CPP_SCHEDULER_H
#define MAIN_CPP_SCHEDULER_H

#include "../process.h"
#include "vector"
#include <cstdlib>
#include <cmath>
#include <cstdio>
#include <algorithm>
#include <queue>
using namespace std;

const int PROCESS_NUM = 20;
const int MAX_QUANTA = 100;


class scheduler {
public:
    scheduler();
    int total_quanta = MAX_QUANTA;
    static bool sortByArrivalTimeFunction (Process a, Process b);
    static bool sortByPriorityFunction (Process a, Process b);
    static bool sortByBurstTimeDecendant (Process a, Process b);
    virtual void run() {};
    void createProcess ();
    void printProceess();
    void sortProcesses(vector<Process> *p, bool sortFunction (Process a, Process b));

    float getAverageWaitTime();
    float getTurnAroundTime();
    void printTimeline();
    int countUnsedProcesses();
    float getThroughput();
    int countTotalRanQuanta();

//    STRATEGY strategy;
protected:
    char timeline[MAX_QUANTA * 2];
    vector<Process> *processes;
};


#endif //MAIN_CPP_SCHEDULER_H
