//
// Created by Thao Ton on 2/20/21.
//

#ifndef UNTITLED_SHORTESTJOBFIRST_H
#define UNTITLED_SHORTESTJOBFIRST_H

#include "scheduler.h"

class shortestJobFirst  : public scheduler {
public:
    void run() override;

private:
    static bool sortByBurstTimeDecendantReference (Process *a, Process *b);
    void sortProcessesReference(vector<Process*> *p, bool sortFunction (Process *a, Process *b));
};


#endif //UNTITLED_SHORTESTJOBFIRST_H
