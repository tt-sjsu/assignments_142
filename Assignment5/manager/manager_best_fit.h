//
// Created by Thao Ton on 3/11/21.
//

#ifndef ASSIGNMENT5_MANAGER_BEST_FIT_H
#define ASSIGNMENT5_MANAGER_BEST_FIT_H

#include "manager.h"

class manager_best_fit: public manager {
public:
    int last_end_index = -1;
protected:
    int get_mem_allocation(int block_size) override;
};


#endif //ASSIGNMENT5_MANAGER_BEST_FIT_H
