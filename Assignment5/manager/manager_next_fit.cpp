//
// Created by Thao Ton on 3/11/21.
//

#include "manager_next_fit.h"
int manager_next_fit::get_mem_allocation(int block_size) {
    int return_start_index = -1;
    int search_start_index, search_end_index;
    int loop = 0;
    search_end_index = MAX_BLOCKS;
    search_start_index = last_end_index < 0 ? 0 : last_end_index + 1;

    while (loop < 2) {
        for(int i = search_start_index; i < search_end_index; i++) {
            if (return_start_index == -1) {
                if (AVALIABLE_MEMORY[i].available) { // free memory
                    return_start_index = i;
                }
            }
            else { // already have a return_start_index index
                if (!AVALIABLE_MEMORY[i].available) { // already occupied
                    return_start_index = -1;
                }
            }

            if (return_start_index >= 0 && i - return_start_index + 1 == block_size) { // found an available block size
                last_end_index = return_start_index + block_size - 1;
                return return_start_index;
            }
        }

        return_start_index = -1;
        search_end_index = last_end_index;
        search_start_index = 0;
        loop++;
    }

    return -1; // not found
}