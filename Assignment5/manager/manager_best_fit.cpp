//
// Created by Thao Ton on 3/11/21.
//

#include "manager_best_fit.h"
#include "vector"
struct Hole {
    int start_index;
    int length;
};

bool sort_by_length(Hole a, Hole b) {
    return a.length < b.length;
}

int manager_best_fit::get_mem_allocation(int block_size) {
    int start = -1;
    vector<Hole> holes;
    for(int i=0; i < MAX_BLOCKS; i++) {
        if (start == -1) {
            if (AVALIABLE_MEMORY[i].available) { // free memory
                start = i;

                if (i == MAX_BLOCKS - 1) {
                    Hole hole{.start_index = start, .length = 1};
                    holes.push_back(hole);
                }
            }
        }
        else { // already have a start index
            if (!AVALIABLE_MEMORY[i].available || i == MAX_BLOCKS - 1) { // already occupied
                Hole hole{.start_index = start, .length = i - start};
                holes.push_back(hole);
                start = -1;
            }
        }
    }

    sort(holes.begin(), holes.end(), sort_by_length);

    for(vector<Hole>::iterator it = holes.begin(); it != holes.end(); it++) {
        if (it->length >= block_size) {
            return it->start_index;
        }
    }


    return -1; // not found
}