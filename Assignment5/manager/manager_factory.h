//
// Created by Thao Ton on 3/11/21.
//

#ifndef ASSIGNMENT5_MANAGER_FACTORY_H
#define ASSIGNMENT5_MANAGER_FACTORY_H
#include "manager.h"

enum STRATEGY {
    FIRST_FIT = 0,
    NEXT_FIT = 1,
    BEST_FIT = 2
};

class manager_factory {
public:
    manager* getManager(STRATEGY strategy);
};


#endif //ASSIGNMENT5_MANAGER_FACTORY_H
