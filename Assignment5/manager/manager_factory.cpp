//
// Created by Thao Ton on 3/11/21.
//

#include "manager_factory.h"
#include "manager_first_fit.h"
#include "manager_next_fit.h"
#include "manager_best_fit.h"

manager* manager_factory::getManager(STRATEGY strategy) {
    manager *m;
    if (strategy == STRATEGY::FIRST_FIT) {
        m = new manager_first_fit();
    }
    else if (strategy == STRATEGY::NEXT_FIT) {
        m = new manager_next_fit();
    }
    else if (strategy == STRATEGY::BEST_FIT) {
        m = new manager_best_fit();
    }
    else {
        m = new manager_first_fit();
    }

    return m;
}