//
// Created by Thao Ton on 3/11/21.
//

#ifndef ASSIGNMENT5_MANAGER_H
#define ASSIGNMENT5_MANAGER_H

#include "globals.h"
#include <stdio.h>
#include <iostream>
#include <unordered_map>
#include <semaphore.h>
#include <queue>

using namespace std;

struct Mem_Prop {
    bool available;
    char name;
    int request_id;
};

struct Requested_Payload {
    char name;
    int id;
    int block_size;
    int start_index;
};

class manager {
private:
    int pipe_fd;
    unordered_map<char, sem_t*> pName_to_sem = {};
    unordered_map<string, Requested_Payload> requestId_to_request = {};

    queue<request_payload> waiting_request;

    int num_allocation_requests_fulfill;
    int num_allocation_requests_fulfill_immediately;
    int num_requests_on_hold;
    int num_requests_before_see_on_hold;
    float percent_block_filled;

    sem_t *get_semaphore_for_process(char name);
    void mem_deallocation(request_payload request);
    bool mem_allocation(request_payload request, bool put_on_hold);
    bool check_waiting_requests();

    void print_memory_map();
    void print_message(string message);

public:
    manager();
    ~manager();

    void start();
    void get_stats();

protected:
    Mem_Prop AVALIABLE_MEMORY[MAX_BLOCKS]; // true means memory is free
    virtual int get_mem_allocation(int block_size) {return -1;};
};


#endif //ASSIGNMENT5_MANAGER_H
