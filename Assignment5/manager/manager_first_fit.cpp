//
// Created by Thao Ton on 3/11/21.
//

#include "manager_first_fit.h"
int manager_first_fit::get_mem_allocation(int block_size) {
    int start = -1;

    for(int i=0; i < MAX_BLOCKS; i++) {
        if (start == -1) {
            if (AVALIABLE_MEMORY[i].available) { // free memory
                start = i;
            }
        }
        else { // already have a start index
            if (!AVALIABLE_MEMORY[i].available) { // already occupied
                start = -1;
            }
        }

        if (start >= 0 && i - start + 1 == block_size) { // found an available block size
            return start;
        }
    }

    return -1; // not found
}
