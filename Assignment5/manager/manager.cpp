//
// Created by Thao Ton on 3/11/21.
//

#include "manager.h"
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>

#include <curses.h>
#include <stdlib.h>
#include <stdbool.h>


#include "synchronize.h"
#include "utils.h"
using namespace utils;
manager::manager() {

    // INITITIALIZE MEMORY ##################
    for (int i=0; i < MAX_BLOCKS; i++) {
        AVALIABLE_MEMORY[i] = Mem_Prop{.available = true};
    }

    num_allocation_requests_fulfill = 0;
    num_allocation_requests_fulfill_immediately = 0;
    num_requests_on_hold = 0;
    num_requests_before_see_on_hold = 0;
    percent_block_filled = 100;

    ///////////////////////////////////////
    // Create a new pipe
    if (mkfifo(FIFOPIPE, 0677) == -1)
    {
        if(unlink(FIFOPIPE) == -1) {
            printf("Fail to unlink.\n");
        }
        if(mkfifo(FIFOPIPE,0677) == -1) {
            printf("Fail to create.\n");
        }
    }

    pipe_fd = open(FIFOPIPE, O_RDONLY);
//    fcntl(pipe_fd, F_SETFL, O_NONBLOCK);
    if(pipe_fd == -1)
    {
        printf("Failed to open the fifo.\n");
        _exit (-1);
    }

}

manager::~manager() {
    close(pipe_fd);
    printf("Done.\n");
}

void manager::start() {

    initscr();
    cbreak();
    noecho();
    clear();

    request_payload request;
    int check;
    bool successful_added = true;
    bool already_saw_on_hold = false;
    while (successful_added || waiting_request.size() != pName_to_sem.size()) { // stop when all processes are in queue
        check = read(pipe_fd, &request, sizeof(request_payload));
        if (check > 0) {
            if (request.type == REQUEST_TYPE::ALLOCATION) {
                successful_added = mem_allocation(request, true);
                if (successful_added) {
                    num_allocation_requests_fulfill_immediately++;
                    if (!already_saw_on_hold) {
                        num_requests_before_see_on_hold++;
                    }
                }
                else {
                    already_saw_on_hold = true;
                }
            }
            else { //MEMORY DEALLOCATION
                mem_deallocation(request);
                successful_added = check_waiting_requests();
                if (!already_saw_on_hold) {
                    num_requests_before_see_on_hold++;
                }
            }
        }
    }

//    //acknowdge all pending requests
//    for (auto it : requestId_to_request) {
//        char name = it.second.name;
//        sem_t *semaphore_wait = get_semaphore_for_process(name);
//        SIGNAL(semaphore_wait);
//    }

    int count = 0;
    for (int i = 0; i < MAX_BLOCKS; i++) {
        if (!AVALIABLE_MEMORY[i].available) {
            count++;
        }
    }

    percent_block_filled = 1.0 * count / MAX_BLOCKS * 100;


    printf("\n\nDone\n");
    print_memory_map();

    getch();
    endwin();
}

sem_t *manager::get_semaphore_for_process(char name) {
    sem_t *semaphore_wait;
    auto it = pName_to_sem.find(name);
    if (it == pName_to_sem.end()) { // not found
        char wait_name[SEMAPHORE_WAIT_LENGTH];
        sprintf(wait_name, "%s_%c", SEMAPHORE_WAIT_PREFIX, name);
        semaphore_wait  = OPEN_MODE(wait_name, O_RDWR);
        pName_to_sem[name] = semaphore_wait;
    }
    else {
        semaphore_wait = it->second;
    }

    return semaphore_wait;
}

void manager::print_memory_map() {
    int x = 0, y = 0;

    for (int i = 0; i < MAX_BLOCKS; i++)
    {
        mvaddch(y, x, AVALIABLE_MEMORY[i].available ? '.' : AVALIABLE_MEMORY[i].name);
        if (++x == ROW_LENGTH)
        {
            x = 0;
            y++;
        }
    }

    refresh();
}

void manager::print_message(string message) {
    mvaddstr(MAX_BLOCKS/ROW_LENGTH + 1, 0, message.c_str());
}


void manager::mem_deallocation(request_payload request) {
    string id_name = "";
    id_name.push_back(request.name);
    id_name.push_back(request.past_request_id);
    auto it = requestId_to_request.find(id_name);
    if (it != requestId_to_request.end()) { // found
        Requested_Payload past_request = it->second;
        for (int i=0; i < request.block_size; i++) {
            AVALIABLE_MEMORY[i + past_request.start_index].available = true;
        }

        char buff[100];
        snprintf(buff, sizeof(buff), "CLEAR %d blocks starting at %d for %c with request_id %d, for past_request_id %d\n",
               request.block_size, past_request.start_index, past_request.name, request.id, past_request.id);
        print_message(buff);
        print_memory_map();
    }
}

bool manager::mem_allocation(request_payload request, bool put_on_hold) {
    bool successful_added = true;
    int start_idx = get_mem_allocation(request.block_size);
    if (start_idx >= 0) { // found free block, get index
        for (int i=0; i < request.block_size; i++) {
            AVALIABLE_MEMORY[i + start_idx] = {.available = false, .name = request.name, .request_id = request.id};
        }

        char buff[100];
        snprintf(buff, sizeof(buff),"SET %d blocks starting at %d for %c with request_id %d\n",
                 request.block_size, start_idx, request.name, request.id);

        print_message(buff);
        print_memory_map();

        string id_name = "";
        id_name.push_back(request.name);
        id_name.push_back(request.id);

        Requested_Payload requested = {.name =  request.name,
                .id =  request.id,
                .block_size =  request.block_size,
                .start_index = start_idx
        };

        // requestId_to_start_index["A10"] = 10
        requestId_to_request[id_name] = requested;

        sem_t *semaphore_wait = get_semaphore_for_process(request.name);
        SIGNAL(semaphore_wait);
        num_allocation_requests_fulfill++;
    }
    else { // memory full

        if(put_on_hold) {
            num_requests_on_hold++;
            waiting_request.push(request);
            char buff[100];
            snprintf(buff, sizeof(buff), "memory is full, putting request %d for %c with block %d on hold\n",
                     request.id, request.name, request.block_size);
            print_message(buff);
        }
        successful_added = false;
        print_memory_map();
    }
    return successful_added;
}

bool manager::check_waiting_requests() {
    bool successful_added = true;
    while (waiting_request.size() > 0 && successful_added) {
        request_payload pending_request = waiting_request.front();
        successful_added = mem_allocation(pending_request, false);
        if (successful_added) {
            waiting_request.pop();
        }
    }
    return successful_added;
}

void manager::get_stats() {
    printf("Allocation requests in total were fulfilled: \t\t%d\n", num_allocation_requests_fulfill);
    printf("Allocation requests fulfilled immediately: \t\t%d\n", num_allocation_requests_fulfill_immediately);
    printf("Allocation requests first put on hold: \t\t%d\n", num_requests_on_hold);
    printf("Requests serviced before an on hold: \t%d\n", num_requests_before_see_on_hold);
    printf("Percentage of the memory blocks were filled: \t%.2f\n", percent_block_filled);
}