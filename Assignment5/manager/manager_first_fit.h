//
// Created by Thao Ton on 3/11/21.
//

#ifndef ASSIGNMENT5_MANAGER_FIRST_FIT_H
#define ASSIGNMENT5_MANAGER_FIRST_FIT_H
#include "manager.h"

class manager_first_fit: public manager {
protected:
    int get_mem_allocation(int block_size) override;

};


#endif //ASSIGNMENT5_MANAGER_FIRST_FIT_H
