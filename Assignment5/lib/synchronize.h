#ifndef SYNCHRONIZE_H_
#define SYNCHRONIZE_H_

#include <semaphore.h>
#include <pthread.h>

// Mutex functions
int INIT(pthread_mutex_t *mutex, char *name);
void LOCK(pthread_mutex_t *mutex, char *name);
void UNLOCK(pthread_mutex_t *mutex, char *name);

// Semaphore functions
void REMOVE(char *name);
sem_t *OPEN(char *name, int value);
void WAIT(sem_t *semaphore, char *name);
void SIGNAL(sem_t *semaphore);
bool WAIT_TRY(sem_t *semaphore, char *name);
void SIGNAL(sem_t *semaphore, char *name);
sem_t *OPEN_MODE(char *name, int mode);

#endif /* SYNCHRONIZE_H_ */
