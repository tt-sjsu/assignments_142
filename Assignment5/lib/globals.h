//
// Created by Thao Ton on 3/11/21.
//

#ifndef ASSIGNMENT5_GLOBALS_H
#define ASSIGNMENT5_GLOBALS_H

#define MAX_BLOCKS  3000
#define MAX_ITERS   1000
#define ROW_LENGTH   100
#define U_SECONDS  50000

#define FIFOPIPE "/tmp/sharedPipe"
#define SEMAPHORE_WAIT_LENGTH 30
#define SEMAPHORE_WAIT_PREFIX "/CMPE142_assignment5"

enum REQUEST_TYPE {
    ALLOCATION,
    DEALLOCATION
};

struct request_payload {
    char name;
    int id;
    int block_size;
    REQUEST_TYPE type;
    int past_request_id; // use for de-allocation
};

const int PRIMES[] = {3, 5, 7, 11, 13, 17, 19, 23, 29};
#define TOTAL_PRIMES 9


#endif //ASSIGNMENT5_GLOBALS_H
