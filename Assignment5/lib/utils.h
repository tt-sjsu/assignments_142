//
// Created by Thao Ton on 3/11/21.
//

#ifndef ASSIGNMENT5_UTILS_H
#define ASSIGNMENT5_UTILS_H
#include "globals.h"
#include <iostream>

namespace utils {
    void print_request(request_payload &payload) {
        string request_type = payload.type == REQUEST_TYPE::ALLOCATION ? "allocation" : "deallocation";
        printf("payload name=%c, id=%d, past_request_id=%d, block_size=%d, type=%s \n", payload.name, payload.id, payload.past_request_id, payload.block_size, request_type.c_str());
    }
}
#endif //ASSIGNMENT5_UTILS_H
