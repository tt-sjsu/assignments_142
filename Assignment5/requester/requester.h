//
// Created by Thao Ton on 3/11/21.
//

#ifndef ASSIGNMENT5_REQUESTER_H
#define ASSIGNMENT5_REQUESTER_H

#include "globals.h"
#include <stdio.h>
#include <iostream>

#include "synchronize.h"
#include <queue>
using namespace std;

struct Requested_Prop {
    int id;
    int block_size;
};

class requester {
private:
    char name;
    int pipe_fd;
    char semaphore_name[SEMAPHORE_WAIT_LENGTH];
    sem_t *wait_semaphore;
    vector<Requested_Prop> requested;
    int current_request_id;
public:
    requester(char name);
    ~requester();

    void start();
    void request_mem_allocation();
    void request_mem_deallocation();
};


#endif //ASSIGNMENT5_REQUESTER_H





