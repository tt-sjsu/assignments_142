//
// Created by Thao Ton on 3/11/21.
//
#include "requester.h"
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>

#include "synchronize.h"
#include "utils.h"
using namespace utils;

requester::requester(char name) {
    current_request_id = 0;
    this->name = name;

    printf("Starting requester %c\n", name);

    sprintf(semaphore_name, "%s_%c", SEMAPHORE_WAIT_PREFIX, name);
    REMOVE(semaphore_name);
    //Create new semaphore
    wait_semaphore  = OPEN(semaphore_name, 0);

    pipe_fd = open(FIFOPIPE, O_WRONLY);
    if(pipe_fd == -1)
    {
        printf("Failed to open the fifo.\n");
        exit(-1);
    }
    printf("Opened.\n");
}

requester::~requester() {
//    printf("Done reading.\n");
    close(pipe_fd);
}

void requester::start() {
    int random_number;
    while (true) {
        random_number = rand() % 10 + 1; // 1 to 10
        if (requested.size() == 0 || random_number <= 6) {
            request_mem_allocation();
        }
        else {
            request_mem_deallocation();
        }
        usleep(100000);
    }
}

void requester::request_mem_allocation() {
    int random_block_size_idx = rand() % (TOTAL_PRIMES - 1) + 0; // 1 to (size - 1)
    int block_size = PRIMES[random_block_size_idx];
    request_payload request{.name =  this->name, .id =  current_request_id, .block_size = block_size,
                            .type = REQUEST_TYPE::ALLOCATION, .past_request_id = -1} ;

    if(write(pipe_fd, &request, sizeof(request_payload)) == -1) {
        printf("Fail to send:\n");
    }
    else {
        print_request(request);
        WAIT(wait_semaphore, semaphore_name);
        Requested_Prop past_request{.id = current_request_id, .block_size = block_size};
        requested.push_back(past_request);

        current_request_id++;
    }
}

void requester::request_mem_deallocation() {
    int random_number = rand() % requested.size() + 0; // 1 to 10
    Requested_Prop past_request = requested.at(random_number) ;
    requested.erase(requested.begin() + random_number);

    request_payload request{.name =  this->name, .id =  current_request_id, .block_size =  past_request.block_size,
                            .type = REQUEST_TYPE::DEALLOCATION, .past_request_id = past_request.id} ;
    if(write(pipe_fd, &request, sizeof(request_payload)) == -1) {
        printf("Fail to send:\n");
    }
    else {
        current_request_id++;
    }
    print_request(request);

}
