## To build
```bash
cmake build .
make
```

## To run
```bash
./manager_run 0 # 0 = first, 1 = next, 2 = best
./requester_run 0
./requester_run 1
./requester_run 2
./requester_run 3
```

## Using g++
```bash
 g++ -std=c++11 -lncurses -I${PWD}/lib -I${PWD}/manager ${PWD}/lib/*.cpp ${PWD}/manager/*.cpp  manager_main.cpp -o manager_run
 g++ -std=c++11  -I${PWD}/lib  ${PWD}/lib/*.cpp ${PWD}/requester/*.cpp requester_main.cpp -o requester_run 
 ```