//
// Created by Thao Ton on 3/11/21.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>
#include <errno.h>
#include <iostream>

#include "requester/requester.h"
using namespace std;
int main(int argc, char *argv[])
{
    int name = atoi(argv[1]);
    if (name < 0 || name > 24) {
        printf("invalid requester id, need to be between 1 -> 23\n");
//        std::cout << "invalid requester id, need to be between 1 -> 23";
        exit(-1);
    }


    requester *my_requester = new requester( name + 'A' );
    my_requester->start();
    return 0;
}