//
// Created by Thao Ton on 3/11/21.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/wait.h>

#include <curses.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>

#include "manager/manager.h"
#include "manager/manager_factory.h"
int main(int argc, char *argv[])
{
    int num = atoi(argv[1]);
    STRATEGY strategy;
    if (num == 1) {
        strategy = STRATEGY::NEXT_FIT;
    }
    else if (num == 2){
        strategy = STRATEGY::BEST_FIT;
    }
    else {
        strategy = STRATEGY::FIRST_FIT;
    }


    manager_factory factory;

    manager *memory_manager = factory.getManager(strategy);

    memory_manager->start();
    memory_manager->get_stats();

    return 0;
}