### Build program
```bash
g++ -std=c++11  -I${PWD}/HDD_Scheduling/*.h ${PWD}/HDD_Scheduling/*.cpp main.cpp -o Assignment7
```
### Run program
```bash
./Assignment7 0 1 #0=FCFS, 1=SSTF, 2=SCAN, 3=C_SCAN, 4=LOOK, 5=C_LOOK | 0= left (!= 0 = right)

mkdir -p outputs/right outputs/left 
./Assignment7 0 0 > outputs/left/FCFS.out.txt
./Assignment7 1 0 > outputs/left/SSTF.out.txt
./Assignment7 2 0 > outputs/left/SCAN.out.txt
./Assignment7 3 0 > outputs/left/C_SCAN.out.txt
./Assignment7 4 0 > outputs/left/LOOK.out.txt
./Assignment7 5 0 > outputs/left/C_LOOK.out.txt


./Assignment7 0 1 > outputs/right/FCFS.out.txt
./Assignment7 1 1 > outputs/right/SSTF.out.txt
./Assignment7 2 1 > outputs/right/SCAN.out.txt
./Assignment7 3 1 > outputs/right/C_SCAN.out.txt
./Assignment7 4 1 > outputs/right/LOOK.out.txt
./Assignment7 5 1 > outputs/right/C_LOOK.out.txt

```