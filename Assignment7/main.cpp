/*
 * main.cpp
 *
 *  Created on: Apr 12, 2021
 *      Author: chaudoan
 */

using namespace std;
#include "HDD_Scheduling/FCFS.h"
#include "HDD_Scheduling/SSTF.h"
#include "HDD_Scheduling/SCAN.h"
#include "HDD_Scheduling/C_SCAN.h"
#include "HDD_Scheduling/LOOk.h"
#include "HDD_Scheduling/C_LOOk.h"
#include "HDD_Scheduling/Scheduling.h"



int main(int argc, char *argv[]) {

    int strategy = atoi(argv[1]);
    int is_right = atoi(argv[2]);
    Scheduling *scheduling;
    if (strategy == 0) {
        cout << "Running FCFS Strategy" << endl;
        scheduling = new FCFS();
    }
    else if (strategy == 1) {
        cout << "Running SSTF Strategy" << endl;
        scheduling = new SSTF();
    }
    else if (strategy == 2) {
        cout << "Running SCAN Strategy" << endl;
        scheduling = new SCAN();
    }
    else if (strategy == 3) {
        cout << "Running C_SCAN Strategy" << endl;
        scheduling = new C_SCAN();
    }
    else if (strategy == 4) {
        cout << "Running LOOK Strategy" << endl;
        scheduling = new LOOK();
    }
    else if (strategy == 5) {
        cout << "Running C_LOOK Strategy" << endl;
        scheduling = new C_LOOK();
    }
    if (!is_right) {
        // set to left
        scheduling->set_direction(false);
    }
    scheduling->run();
   return 0;
}