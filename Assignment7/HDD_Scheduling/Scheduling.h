//
// Created by Chau Doan on 4/12/21.
//

#ifndef ASSIGNMENT_7_SCHEDULING_H
#define ASSIGNMENT_7_SCHEDULING_H

#include "vector"
#include <cstdlib>
#include <cmath>
#include <cstdio>
#include <algorithm>
#include <queue>
#include <iostream>
#include <stdlib.h>

using namespace std;
const int max_size = 5000;
class Scheduling {
private:
    int total_move = 0;
protected:
    bool to_right;
public:
    Scheduling();
//    int FIFO[10] = {98, 183, 37, 122, 14, 124, 65, 67, 67, 67};
//    int head = 53;
    int FIFO[10] = {2055, 1175, 2304, 2700, 513, 1680, 256, 1401, 4922, 3692};
    int head = 2255;
    void print_scale();
    void add_movement(int move);
    void print_process(int number);
    virtual void run();
    void print_movement();
    void set_direction(bool to_right);
};


#endif //ASSIGNMENT_7_SCHEDULING_H
