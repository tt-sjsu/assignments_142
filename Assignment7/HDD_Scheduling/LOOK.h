//
// Created by Chau Doan on 4/13/21.
//

#ifndef ASSIGNMENT_7_LOOK_H
#define ASSIGNMENT_7_LOOK_H
#include "Scheduling.h"


class LOOK :public Scheduling{
public:
    void run() override;
};


#endif //ASSIGNMENT_7_LOOK_H
