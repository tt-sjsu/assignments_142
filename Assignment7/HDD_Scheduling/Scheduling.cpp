//
// Created by Chau Doan on 4/12/21.
//

#include "Scheduling.h"

void Scheduling::run() {}

void Scheduling::print_scale()
{
    cout << 0;
    for(int i = 1; i < 6; i++ )
    {
        for (int j = 0; j < 49; j++) {
            cout << " ";
        }
        cout << i;
    }
    cout << endl;
    cout << 0;
    for(int i = 1; i <= 50; i++)
    {
        cout << "...." << i%10 ;
    }

    cout<< endl;
    print_process(head);

}



void Scheduling::set_direction(bool to_right)
{
    this->to_right = to_right;
}

void Scheduling::add_movement(int move)
{
    total_move += move;
}

void Scheduling::print_process(int number)
{
    for(int i = 0; i <= (number/20); i++)
        cout << " ";
    cout<< "*" << number << endl;
}

void Scheduling::print_movement()
{
    cout << "Total head movement: " << total_move << " cylinders" << endl;
    cout<< "\n"<<endl;
}

Scheduling::Scheduling() {
    set_direction(true);
}
