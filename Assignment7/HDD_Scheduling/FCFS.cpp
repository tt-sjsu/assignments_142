//
// Created by Chau Doan on 4/12/21.
//

#include "FCFS.h"

void FCFS::run()
{
    print_scale();
    int seek_count = 0;
    int distance, cur_track;
    for (int i = 0; i < 10; i++) {
        cur_track = FIFO[i];

        // calculate absolute distance
        add_movement(abs(cur_track - head));

        // accessed track is now new head
        head = cur_track;
        print_process(FIFO[i]);
    }
    print_movement();
}