//
// Created by Chau Doan on 4/13/21.
//

#ifndef ASSIGNMENT_7_C_SCAN_H
#define ASSIGNMENT_7_C_SCAN_H
#include "Scheduling.h"

class  C_SCAN: public Scheduling {
public:
    void run() override;
};


#endif //ASSIGNMENT_7_C_SCAN_H
