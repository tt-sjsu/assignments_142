//
// Created by Chau Doan on 4/13/21.
//

#include "SSTF.h"

void SSTF:: sort_schedule()
{
    int temp = head;
    for (int i= 0; i< 9; i++)
    {
        int small_distance = abs(FIFO[i] -temp);
        int idx= i;
        for (int j=i+1; j<10; j++)
        {
            if (small_distance > abs(FIFO[j] -temp))
            {
                idx=j;
                small_distance = abs(FIFO[j] - temp);
            }

        }
        temp = FIFO[idx];
        FIFO[idx] = FIFO[i];
        FIFO[i] = temp;
    }
}
void SSTF::run()
{
    print_scale();
    sort_schedule();
    int seek_count = 0;
    int  cur_track;
    for (int i = 0; i < 10; i++)
    {
        cur_track = FIFO[i];

        // calculate absolute distance
        add_movement(abs(cur_track - head));

        // accessed track is now new head
        head = cur_track;
        print_process(FIFO[i]);
    }
    print_movement();
}