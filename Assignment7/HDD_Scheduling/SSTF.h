//
// Created by Chau Doan on 4/13/21.
//

#ifndef ASSIGNMENT_7_SSTF_H
#define ASSIGNMENT_7_SSTF_H
#include "Scheduling.h"

class SSTF: public Scheduling{
public:
    void run() override;
    void sort_schedule();

};


#endif //ASSIGNMENT_7_SSTF_H
