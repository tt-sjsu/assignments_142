//
// Created by Chau Doan on 4/12/21.
//

#ifndef ASSIGNMENT_7_FCFS_H
#define ASSIGNMENT_7_FCFS_H
#include "Scheduling.h"

class FCFS: public Scheduling {
public:
    void run() override;
    int calculate_movement();
};


#endif //ASSIGNMENT_7_FCFS_H
