//
// Created by Chau Doan on 4/13/21.
//

#ifndef ASSIGNMENT_7_SCAN_H
#define ASSIGNMENT_7_SCAN_H
#include "Scheduling.h"

class SCAN: public Scheduling {
public:
    void run() override;
};


#endif //ASSIGNMENT_7_SCAN_H
