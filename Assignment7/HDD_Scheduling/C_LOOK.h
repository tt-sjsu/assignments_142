//
// Created by Chau Doan on 4/13/21.
//

#ifndef ASSIGNMENT_7_C_LOOK_H
#define ASSIGNMENT_7_C_LOOK_H
#include "Scheduling.h"

class C_LOOK: public Scheduling  {
public:
    void run() override;
};


#endif //ASSIGNMENT_7_C_LOOK_H
