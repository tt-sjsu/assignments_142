//
// Created by Chau Doan on 4/13/21.
//

#include "LOOK.h"
void LOOK:: run()
{
    print_scale();
    int cur_track;
    vector<int> left, right;

    for (int i = 0; i < 10; i++)
    {
        if (FIFO[i] < head)
            left.push_back(FIFO[i]);
        if (FIFO[i] > head)
            right.push_back(FIFO[i]);
    }
    if (to_right) {
        std::sort(left.rbegin(), left.rend());
        std::sort(right.begin(), right.end());
    }
    else {
        std::sort(left.rbegin(), left.rend());
        std::sort(right.begin(), right.end());
    }

    int run = 2;
    while (run--) {
        if (!to_right) {
            for (int i = 0; i < left.size(); i++) {
                cur_track = left[i];
                print_process(cur_track);
                add_movement(abs(cur_track - head));
                head = cur_track;
            }
        }
        else{
            for (int i = 0; i < right.size(); i++) {
                cur_track = right[i];
                print_process(cur_track);
                add_movement(abs(cur_track - head));
                head = cur_track;
            }
        }
        to_right = !to_right;
    }
    print_movement();
}